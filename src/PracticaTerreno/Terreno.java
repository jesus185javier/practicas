package PracticaTerreno;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesus
 */
public class Terreno {
    private int numterreno;
    private float ancho;
    private float largo;
    
    public Terreno(){
    //hace referencia a la clase y a los datos
    this.numterreno=0;
    this.ancho=0.0f;
    this.largo=0.0f;
    }
    //contructor por argumentos
public Terreno(int numterreno,float ancho,float largo){
    this.numterreno=numterreno;
    this.ancho=ancho;
    this.largo=largo;
}    
//copia
public Terreno(Terreno otro){
    this.numterreno=otro.numterreno;
    this.ancho=otro.ancho;
    this.largo=otro.largo;
}

//metodo ser y get
    public int getNumterreno() {
        return numterreno;
    }

    public void setNumterreno(int numterreno) {
        this.numterreno = numterreno;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
//metodo de comportamiento
    public float calcularPerimetro(){
        float calcularPerimetro=0.0f;
        calcularPerimetro=(this.ancho*this.largo)*2;
        return calcularPerimetro;
    }
    
    public float calcularArea(){
        float calcularArea=0.0f;
        calcularArea=this.ancho*this.largo;
        return calcularArea;
    }
}
