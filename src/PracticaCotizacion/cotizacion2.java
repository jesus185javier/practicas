package PracticaCotizacion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesus
 */
public class cotizacion2 {
  //atribulos de la clase
    private int numCotizacion;
    private String descripcion;
    private float precio;
    private float porcentajeInicial;
    private int plazo;
    private float pagoInicial;
    private int total;
    
    public cotizacion2(){
        this.numCotizacion=0;
        this.descripcion="";
        this.precio=0.0f;
        this.porcentajeInicial=0.0f;
        this.plazo=0;
    }
    //constructor por argumentos
    public cotizacion2(int numCotizacion ,String descripcion,float precio,float porcentajeInicial,int plazo){
        this.numCotizacion=numCotizacion;
        this.descripcion=descripcion;
        this.precio=precio;
        this.porcentajeInicial=porcentajeInicial;
        this.plazo=plazo;
    }
    //copia
    public cotizacion2(cotizacion2 otro){
        this.numCotizacion=otro.numCotizacion;
        this.descripcion=otro.descripcion;
        this.precio=otro.precio;
        this.porcentajeInicial=otro.porcentajeInicial;
        this.plazo=otro.plazo;
    }

    //metodo set y get
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeInicial() {
        return porcentajeInicial;
    }

    public void setPorcentajeInicial(float porcentajeInicial) {
        this.porcentajeInicial = porcentajeInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    //metodo de comportamiento
    public float calcularPagoInicial(){
        float pagoInicial=0.0f;
        pagoInicial=this.precio*this.porcentajeInicial;
        return pagoInicial;
    }
   public float calcularTotal(){
       float total=0.0f;
       pagoInicial=this.precio*this.porcentajeInicial;
       total=this.precio-this.pagoInicial;
       return total;
   }
    public float calcularPagoMensual(){
        float total=0.0f;
        float pagoInicial=0.0f;
        float pagoMensual=0.0f;
        pagoInicial=this.precio*this.porcentajeInicial;
        total=this.precio-this.pagoInicial;
        pagoMensual=this.total/this.plazo;
        return pagoMensual;
    }
    
}
