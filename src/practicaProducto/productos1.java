/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicaProducto;

/**
 *
 * @author jesus
 */
public class productos1 {
    //atributos de la clase
    private int codProd;
    private String desc;
    private float pc;
    private float pv;
    private String um;
    private int cantProd;
    
    public productos1(){
     this.codProd=0;
     this.desc="";
     this.pc=0.0f;
     this.pv=0.0f;
     this.um="";
     this.cantProd=0;
    
    }
    //contructor por argumentos 
    public productos1(int codProd,String desc, float pc, float pv,String um, int candProd){
        this.codProd=codProd;
        this.desc=desc;
        this.pc=pc;
        this.pv=pv;
        this.um=um;
        this.cantProd=cantProd;
    }
    //copia
    public productos1(productos1 otro){
        this.codProd=otro.codProd;
        this.desc=otro.desc;
        this.pc=otro.pc;
        this.pv=otro.pv;
        this.um=otro.um;
        this.cantProd=otro.cantProd;
    }
//set y get
    public int getCodProd() {
        return codProd;
    }

    public void setCodProd(int codProd) {
        this.codProd = codProd;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPc() {
        return pc;
    }

    public void setPc(float pc) {
        this.pc = pc;
    }

    public float getPv() {
        return pv;
    }

    public void setPv(float pv) {
        this.pv = pv;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public int getCantProd() {
        return cantProd;
    }

    public void setCantProd(int cantProd) {
        this.cantProd = cantProd;
    }
    //metodo de comportamiento
    public float calculoPrecioVenta(){
        float calculoPrecioVenta=0.0f;
        calculoPrecioVenta=this.pv*this.cantProd;
        return calculoPrecioVenta;
    }
    public float calculoPrecioCompra(){
        float calculoPrecioCompra=0.0f;
        calculoPrecioCompra=this.cantProd*this.pc;
        return calculoPrecioCompra;          
    }
    
    public float calculoGanancia(){
        float calculoGanancia=0.0f;
        calculoGanancia=(this.pv-this.pc);
        return calculoGanancia;
    }

}

